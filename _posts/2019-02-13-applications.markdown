---
layout: post
title:  "完整的解决方案"
date:   2019-02-13
---
市面上常见的应用软件销售方式有2种，针对大型企业的大型销售，特点是比较昂贵;还有一种是伴随着云计算的SaaS软件，这里关注的是ERP，所以谈到的其实是SaaS里的ERP类。针对大型企业就不谈了，SaaS这种方式购买软件有什么优缺点呢？

优点：

1. 订阅制，定价透明，能事先算出需要花多少钱。 
2. 每用户价格比较低。方便除大型企业之外的企业选用。

缺点：

1. 有数字化需求的企业通常通常有一定规模，花销通常比较昂贵
2. SaaS需要一定的定制才能使用，不能开箱就用，需要IT支持才能用。

市场上需要给企业提供一个低价选项，从底层架构到应用程序全部采用成熟的Open Source软件，以降低花销，因为以企业使用数据为重点，要求定制少以降低费用，快速实施。采用云服务安装，例如AWS。

主要特点： 

1. 用户端可以使用移动设备，处理订单，如使用平板电脑，面对Web界面。

2. 总部端用Web界面处理数据，老板用Dashboard了解整体情况。公司销售向Omnichannel，以活动（event）为中心转型，对数据使用的需求会增加，ERP是基础工具。

3. 企业欲建立竞争优势，需要专注于业务，企业不宜为IT增加成本。

**以下是详细介绍：**

云服务器

- 以AWS为例，采用t2.micro，1VCPU，1 Gb RAM，Ubuntu 14.04 HVM，AWS北京，每月花销在75元RMB，这个配置运行小型使用场合没有问题。

- 只采用EC2服务器，不采用其他服务。

- EC2里只采用Elastic IP，Security Group，为简化服务，只采用这两项设置，由IT支持在服务器端设置好，用户无关。

- 服务器操作系统采用Ubuntu 14.04以上，以便下一步安装Docker。

Docker

这是简化安装的关键，在这个服务器里，所要安装的应用软件odoo和数据库postgres都采用container安装。Docker安装具体方法参照docker文档。


Odoo

这是应用软件，原名OpenERP。Odoo是开源的，使用时不花钱，我们对窗体，菜单，dashboard，有基本的定制。采用POS界面，用户端是店铺界面，公司汇总端有Web界面。界面是本地化的，比如要繁体中文，有繁体中文界面。界面的文字可以翻译修改。默认界面是英文的。

```
1. Docker简化安装举例：（假设Docker已安装）

1.1下载 docker images（打开terminal，ssh进入云服务器）

docker pull odoo/odoo：11 
docker pull postgres:9.3  

1.2 运行docker container（即运行应用程序）

\\运行postgres数据库container

docker run -d -P --name dbsample \
	--restart=always \
	-v $(pwd)/yourpath:/var/lib/postgresql/backup \
	postgres:9.3

\\运行odoo container

docker run -p xxxx:8069 --name="odooapp" \
	--restart=always \
	-v $(pwd)/odoo_addons:/opt/odoo/additional_addons \
	--link dbsample:db \
	odoo/odoo:11.0 start

 - xxxx 代表你选的端口号

1.3 以往普通安装，在linux里安装postgres数据库连同应用程序，文档长不说，还难于避免不同人造成失误。总之docker的出现大大改变了IT工作方式，简化了安装，方便部署。
```

**维持竞争力的几点思考：**

1. 这套方案能显著降低对人员的要求，企业能得到低价的选项，IT服务能简化服务的复杂性。

2. Docker着重用于Open Source软件，docker对安装的简化使低价成为可能。

3. 从底层到应用软件都采用open source，为企业省钱。

4. 云服务器在企业范围内才刚刚兴起，未来还有普及需要，云服务是趋势。

5. 这样，主要ERP的工作都在定制上，重要工作都在云服务器上，现场需要个支持工程师，所需技能大大降低了。要达成快速部署，低价的目的，在实施上不因拖延而被拖进去。这决定了不能以传统产品方式销售，以符合云计算普及背景下的方式销售，快速接触，快速退出。

6. 用户对数据的理解是这个方案的重点，企业数字化的主题远超越软件，需要理解变革的要求，从组织方式，企业宗旨这些企业存在的基础问题去考虑。


**结语**

基于Open Source的软件会显著降低企业的IT投入，企业还需对数据有深入的理解，从数据中顺应市场的变化，避免软件成为作业的模板。应从数据角度思考市场遇到的业务问题。